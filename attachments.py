import os
import logging
from pathlib import Path
import my_logging

logging.basicConfig(
    filename='Gautomator.log',
    filemode='a',
    level=logging.INFO,
    format='%(asctime)s    %(levelname)s    %(funcName)s   %(message)s   Line:%(lineno)d',
    datefmt='%Y-%m-%d %H:%M:%S')

class Attachment:
    """ Discription: class saves and validates attachements
            
        Attributes: 
        email_part: obj = 
        name: str = 
        """   
    def __init__(self, email_part, name):
        self.email_part=email_part
        self.name=name
        self.test=False
        self.df=None
        self.df_result=False

    def save(self, attachment_dir):
        """ Discription: method saves attachment to directory

            Arguements: 
            attachment_dir: str = local directory where attachment will be downloaded to.
            email_part: pseudo-dictionary = 
            f: stream obj = opens stream and takes in string (location and filename)

            Return: Writes email_part to local memory 
            """   
        if isinstance(self.name, str):
            self.file_path=os.path.join(attachment_dir, self.name)
            with open(self.file_path, 'wb') as f:
                f.write(self.email_part.get_payload(decode=True))
        else:
            logging.error(f"{__class__.__name__}   Attachment download failed")

    def validate(self, df, min_rows, min_columns):
        """ Discription: method validates attachment within directory based on rows and columns requirement.

            Arguements: 
            attachment_dir: str = local directory where attachment will be downloaded to.
            email_part: pseudo-dictionary = 
            f: stream obj = opens stream and takes in string (location and filename)

            Return: df_reult attribute as either True or False
            """   
        if df.shape[0] > min_rows and df.shape[1] > min_columns:
            self.df_result = True
            logging.error(f"{__class__.__name__}   Attachment file {self.email_part.get_filename()} PASSED validation")
        else:
            logging.error(f"{__class__.__name__}   Attachment file {self.email_part.get_filename()} FAILED validation")


