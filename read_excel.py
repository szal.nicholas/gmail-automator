import pandas as pd
import os
import logging
import numpy

logging.basicConfig(
    filename='Gautomator.log',
    filemode='a',
    level=logging.INFO,
    format='%(asctime)s    %(levelname)s    %(funcName)s   %(message)s   Line:%(lineno)d',
    datefmt='%Y-%m-%d %H:%M:%S')

class Excel:
    """ Discription: class opens excel files and manipulates the datasets within.
            
        Attributes: 
        path: str = path of input excel file
        df_agro: list = contains pandas agrogated dataframe
        df_weely: list = contains pandas manipulated dataframe
        df_final: list = contains pandas fully formatted dataframe
        """  
    run_once=False
    def __init__(self, file_dir, file_name):
        self.path= os.path.join(file_dir, file_name)
        self.df_aggro= []
        self.df_weekly = []
        self.df_final = []
        

    def add_df(self, save_filepath):
        """ Discription: opens single excel file performs aggrigation and sumation on desired fields 

            Arguements: 
            save_filepath: string = directory with name of file to be created to store final df_final.

            Return: attenuates df_final to created csv file.
            """   
        try:
            df = pd.read_excel(self.path)
            logging.info (f'{__class__.__name__}  {os.path.basename(self.path)} Analyzed')
            if any(df.columns.str.contains("Sales price/unit")):
                df["Total sales"] = df['Quantity sold'] * df['Sales price/unit'] #calculate total sales
                df_agro = (df[['Date', 'Quantity sold', 'Total sales']]).copy() #aggrigate data and add total sales column
                df_agro['Date'] = pd.to_datetime(df_agro['Date']) #change dates to datetime64 format
                df_weekly = df_agro.resample('W', on='Date').sum().round(decimals=2).reset_index().sort_values(by='Date') #resample data to show sales accumaluted every week.
                df_final=df_weekly[["Date","Quantity sold","Total sales"]].replace(0, numpy.NaN) # weeks with no values have NaN applied
                print(f'- {os.path.basename(self.path)}: Analyzed')
                if __class__.run_once is False:
                    df_final.to_csv(save_filepath, header=True, index=None, sep=' ', mode='a') #saves data to current csv file, new data gets appended
                    __class__.run_once = True
                else: 
                    df_final.to_csv(save_filepath, header=None, index=None, sep=' ', mode='a') #saves data to current csv file, new data gets appended
                logging.info (f'{__class__.__name__}  {os.path.basename(self.path)} Analyzed')
            else:
                print(f'- {os.path.basename(self.path)}: Doesnt fit required column-name criteria')
                logging.info(f"{__class__.__name__}  {os.path.basename(self.path)} Processed!'")

        except Exception as error_msg:
            logging.error(f'{__class__.__name__}  {(error_msg)}')
            raise