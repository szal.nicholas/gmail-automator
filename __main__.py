from connection import Connection
from exceptions import SMTPConnectionError, EmailScrapingError, ReadExcelError, GenerateGraphError
from mail import Mailbox, Mail
from attachments import Attachment
from read_excel import Excel
import pandas as pd
import os
import sys
import datetime
from gen_graph import Graph
from pathlib import Path

USER_EMAIL = os.environ.get('GMAIL_ADD')
PW = os.environ.get('GMAIL_PW')
IMAP_URL = 'imap.gmail.com'
BASE_DIR = Path(__file__).resolve().parent
FILE_LOCATION = os.path.join(BASE_DIR, 'Attachments_Repository/')
FINISHED_LOCATION = os.path.join(BASE_DIR, 'Processed_Data/')

Path(FILE_LOCATION).mkdir(parents=True, exist_ok=True)
Path(FINISHED_LOCATION).mkdir(parents=True, exist_ok=True)

MAIL_FOLDER = 'INBOX'
count = 0

try:
    server = Connection(IMAP_URL)
    server.authenticate(USER_EMAIL, PW)
    server.test(server.result) 
except SMTPConnectionError:
    print("FATAL ERROR: appprogrammer has been notified. please stand by..... \nHINT: Try to turn 'less secure app access' to ON for your gmail account ")
    sys.exit()

try:
    mailbox = Mailbox(folder_name=MAIL_FOLDER)
    mailbox.count_massage(server.SMTP)
    mailbox.get_email(server.SMTP)
    for message in mailbox.ls_messages: 
        mailbox.mail = Mail(msg=message[0][1])
        mailbox.mail.convert_bytes_to_email_obj(bytes_email=mailbox.mail.msg)
        mailbox.mail.get_body(mailbox.mail.email_obj)

        for email_part in mailbox.mail.email_obj.walk():
            file_name = email_part.get_filename() #returns name of attachments
            
            if isinstance(file_name, str) and '.xlsx' in file_name:
                attachment = Attachment(email_part, file_name)
                attachment.save(FILE_LOCATION)
                attachment.df = pd.read_excel(attachment.file_path)
                attachment.validate(attachment.df, 2, 3) # define dataframe, min number of required rowes, min number of columns.
                count+=1
                print(f'- Attachment {count}: {file_name} | Validation Passed: {attachment.df_result}')
    print('- Email data retrieved successfully')
except EmailScrapingError: 
    print('FATAL ERROR: Email information retrieval unsuccessful. programmer has been notified. please stand by.....')
    sys.exit()

try:
    processed_df_dir=os.path.join(FINISHED_LOCATION, datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))

    for file in os.listdir(FILE_LOCATION):
        if file.endswith(".xlsx"):
            process_df = Excel(FILE_LOCATION, file)
            process_df.add_df(processed_df_dir)

except ReadExcelError(Exception):
        print('FATAL ERROR: Cannot process excel files')
        sys.exit()

try:
    get_graph = Graph(processed_df_dir)
    get_graph.generate()
except GenerateGraphError(Exception):
        print('FATAL ERROR: Cannot generate graph')
        sys.exit()