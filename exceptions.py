

#custom exception for specific error. 
class SMTPConnectionError(Exception): 
    pass

class EmailScrapingError(Exception):
    pass

class ReadExcelError(Exception): 
    pass

class GenerateGraphError(Exception): 
    pass