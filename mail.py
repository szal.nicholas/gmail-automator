import email
import logging

class Mailbox:
    """ Discription: Pulls the amount of messages from the specified mailbox folder.

        Attributes:
        folder_name: obj = Name of the folder within the mailbox ex: INBOX, SPAM.
        message_count' int = Amount of messages within selected mailbox folder.
        ls_messages lst = List of messages within the the selected mailbox folder.
    """
    def __init__(self, folder_name):
        self.folder_name=folder_name
        self.message_count=0
        self.ls_messages=[]
    def count_massage(self, SMTP):
        try:
            self.message_count = int(SMTP.select(self.folder_name)[1][0])
            logging.info(f"{__class__.__name__}  Number of Emails in Mailbox:{self.message_count}")
        except Exception as error_msg:
            logging.error(f"{__class__.__name__}  {str(error_msg)}")
            raise
    def get_email(self, SMTP):
        """ Discription: method appends data to ls_message list.
            
            Arguments: 
            ls_messages: list = 
            data: list =

            Return: ls_messages object with element appended.
            """        
        try:
            for index in range(1, self.message_count+1):
                typ, data = SMTP.fetch(str(index),'(RFC822)') #fetches email body
                self.ls_messages.append(data)
        except Exception as error_msg:
            logging.error(f"{__class__.__name__}  {str(error_msg)}")
            raise

class Mail:
    """ Discription: Identifies emails payload, message will be decoded if message header is quoted-printable or Base64.

        Attributes:
        msg: obj) = 
        email_obj: pseudo-dictionary = 
        body: pseudo-dictionary = 
        """
    def __init__(self, msg):
        self.msg=msg
        self.email_obj=None
        self.body=None
    def convert_bytes_to_email_obj(self, bytes_email):
        """ Discription: Method converts bytes object to email.message.Message object. Creates an instance attribute email_obj
            
            Arguments: 
            bytes_email: bytes = 

            Return: Creates an instance attribute email_obj.
            """
        try:
            self.email_obj=email.message_from_bytes(bytes_email)
        except Exception as error_msg:
            logging.error(f"{__class__.__name__}  {str(error_msg)}")
            raise
    def get_body(self, email_obj):
        """ Discription: Method extracts payload from email object and creates a body attribute.
            
            Arguments: 
            email_obj: pseudo-dictionary = contains email message consisting of headers and a payload. 
                Headers must be RFC 5322 style names and values.
            body: pseudo-dictionary = 

            Return: Creates body attribute.
            """
        try:
            if email_obj.is_multipart():
                self.body = email_obj.get_payload(0,False)
            else: 
                self.body = email_obj.get_payload(None,True)
        except Exception as error_msg:
            logging.error(f"{__class__.__name__}  {str(error_msg)}")
            raise
