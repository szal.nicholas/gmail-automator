import os
from pathlib import Path
import logging

class Logger:
    def __init__(self):
        logging.basicConfig(
            filename=os.path.join(Path(__file__).resolve().parent , 'Gautomator.log'),
            filemode='a',
            level=logging.INFO,
            format='%(asctime)s    %(levelname)s    %(funcName)s   %(message)s   Line:%(lineno)d',
            datefmt='%Y-%m-%d %H:%M:%S')
        

    

    