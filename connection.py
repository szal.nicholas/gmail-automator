import imaplib
import logging
from exceptions import SMTPConnectionError

logging.basicConfig(
    filename='Gautomator.log',
    filemode='a',
    level=logging.INFO,
    format='%(asctime)s    %(levelname)s    %(funcName)s   %(message)s   Line:%(lineno)d',
    datefmt='%Y-%m-%d %H:%M:%S')

class Connection:
    """ Discription: class establishes connection with SMTP email server via SSL encrypted socket.
            
        Attributes: 
        SMTP: obj = communicates with IMAP4 API 
        url: str = gmail url

        Return: successful SSL connection to gmail url.
        """   
    def __init__(self, url):
        try:
            self.SMTP = imaplib.IMAP4_SSL(host=url, timeout=5)
        except imaplib.IMAP4.error:
            logging.error(f"{__class__.__name__} Connection to SMTP server timed out, check url.")
        except Exception as error_msg:
            logging.debug(str(error_msg))
            raise
    def authenticate(self, user, pw):
        """ Discription: method logs user into email account using user email and password credentials

            Arguements: 
            user: str = user email address
            pw: str = user password

            Return: recieves a message back from the server to confirm successful login.
            """   
        try:
            self.result = self.SMTP.login(user, pw)
            logging.info(f"{__class__.__name__} Authentication Status: {str(self.result[1][0]).split(' ')[-1][:-1]}")
        except imaplib.IMAP4.error as error_msg:
            self.result=None
            logging.error(f"{__class__.__name__} {(error_msg)}")
        except Exception as error_msg:
            self.result=None
            logging.debug(str(error_msg))
            raise
    def test(self, result):
        """ Discription: method displays and logs connection status

            Arguements: 
            result: tuple = connection status

            Return: logs connection status or error.
            """   
        if isinstance(result, tuple):
            self.status = result[0]
            print('- SMTP connection successful')
            logging.info(f"{__class__.__name__} Connection Status: {self.status}")
        else: 
            logging.error(f"{__class__.__name__} {type(result)} type out of function scope")
            raise SMTPConnectionError('Connection Failed')