import pandas as pd
import matplotlib.pyplot as plt 
import logging

logging.basicConfig(
    filename='Gautomator.log',
    filemode='a',
    level=logging.INFO,
    format='%(asctime)s    %(levelname)s    %(funcName)s   %(message)s   Line:%(lineno)d',
    datefmt='%Y-%m-%d %H:%M:%S')

class Graph:
    """ Discription: class created for graph generation.
            
        Attributes: 
        wkly_csv_file_loc: string = location of processed csv file
        df: list = pulled dataframe

        Return: wkly_csv_dir with directory string
        """   
    def __init__ (self, wkly_csv_dir):
        self.wkly_csv_dir=wkly_csv_dir
        self.df=[]

    def generate(self):
        """ Discription: method plots and saves dataframe within csv file

            Arguements: 
            ax: tuple = defines figure-level attributes and save figure as an image
            fig: tuple = holds figures of the axes objects
            xy1: dataframe = dataframe with NaN weeks masked in order to apply connection line between points. 


            Return: plotted dataset to graph as png file.
            """   
        try:
            p_font = {'fontname':'Papyrus'}
            self.df = pd.read_csv(self.wkly_csv_dir, delimiter = ' ') #open csv file
            self.df["Date"] = pd.to_datetime(self.df["Date"]) #sort rows by date
            x = self.df['Date'] #aggrogate Date column as "x"
            y = self.df['Total sales'] #aggrogate Total sales column as "y"
            
            fig, ax=plt.subplots(dpi=128, figsize=(13,6)) #subplot(nrows, ncols, index, **kwargs)
            ax.grid(color='white', linestyle='-', linewidth=1) #apply white gridlines
            fig.autofmt_xdate() # puts a slight angle on the x-axis tick labels
            ax.margins(x=0)
            ax.set_ylim(ymin=0, ymax=y. max()*1.2) # y-axis max has +20% padding
            plt.tick_params(axis='both', which='major', labelsize=8) # specify size of labels
            ax.spines['top'].set_color('lightgrey') #set graph top spine color
            ax.spines['right'].set_color('lightgrey') #set graph right spine color
            
            ax.scatter(x,y, s=6) #generate scatter plot and set marker size s=6
            xy1=self.df.dropna()#maks NaN values in order to apply connection to the 
            ax.plot(xy1['Date'],xy1['Total sales'],linewidth=1) #plot masked value to complete line between points

            ax.fill_between(xy1['Date'], xy1['Total sales'], color="lightblue", alpha=0.5) # color fill underneath plot line.
            ax.yaxis.set_major_formatter('${x:1,.0f}') #applies y-axis with dollar format
            ax.set_title('INCREMENTAL   SALES', **p_font, fontsize=30, color= '#666C7C', fontweight='bold') #
            ax.set_ylabel('USD') #Add y-axis label
            fig.tight_layout(pad=2, w_pad=0.5, h_pad=1.0) #tightens graph layout
            ax.set_facecolor('#ECECEC') #set color of graph area background

            fig.savefig(self.wkly_csv_dir+'_graph') #saves graph to png

        except Exception as error_msg:
            logging.error(f'{__class__.__name__}  {(error_msg)}')
            raise
        
        else: 
            print(f'- Graph with accumulated data generated')
            logging.info(f"{__class__.__name__} Graph processed and saved to: {self.wkly_csv_dir+'_graph'}")